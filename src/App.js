import logo from './logo.svg';
import './App.css';
import BTState from './BTState/BTState';

function App() {
  return (
    <div>
        <BTState />
    </div>
  );
}

export default App;
