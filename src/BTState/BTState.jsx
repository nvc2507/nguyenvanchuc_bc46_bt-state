import React, { useState } from "react";
import data from "./dataGlasses.json";


const BTState = () => {
  
  
  const [sp, setSp] = useState(data[0]);
  const handlCheck = (pick) => {
    setSp(data[pick]);
  };
  return (
    <div >
      <h3
        className="text-center"
        style={{
          fontSize: '32px',
          color: "white",
          backgroundColor: "black",
          opacity: "0.5",
        }}
      >
        Try Glasses App Online
      </h3>
      
      <div id="background">
        <div className="row">
          <div
            className="card col-6"
            style={{ alignItems: "center", border: "none"}}
          >
            <div className="card-body" style={{ position: 'relative'}}>
              <img
                className="img-fluid"
                src="../glassesImage/model.jpg"
                alt=""
                style={{ width: "350px", height: "400px" }}
              />
              <img 
              
              style={{
                width: "200px",
                height: "70px",
                
                position: "absolute",
                top: "114px",
                left: '96px',
                border: "none",
                
                opacity: '0.7'
              }}
              src={sp.url} alt="" />
              <div
                className="card-footer"
                style={{
                  width: "350px",
                  height: "170px",
                  color: 'white',
                  position: "absolute",
                  bottom: "12px",
                  border: "none",
                  backgroundColor: "#AA0000",
                  opacity: '0.6'
                }}
              >
                <p className="font-weight-bold">{sp.name}</p>
                <p>{sp.price}$</p>
                <p>{sp.desc}</p>
              </div>
            </div>
          </div>
          <div
            className="card col-6"
            style={{ alignItems: "center", border: "none" }}
          >
            <div className="card-body " style={{ position: 'relative'}}>
              <img
                className="img-fluid"
                src="../glassesImage/model.jpg"
                alt=""
                style={{ width: "350px", height: "400px" }}
              />
              <img 
              
              style={{
                width: "200px",
                height: "70px",
                
                position: "absolute",
                top: "114px",
                left: '96px',
                border: "none",
                
                opacity: '0.7'
              }}
              src={sp.url} alt="" />
              {/* <div
                className="card-footer"
                style={{
                  width: "350px",
                  height: "170px",
                  color: 'white',
                  position: "absolute",
                  bottom: "12px",
                  border: "none",
                  backgroundColor: "#AA0000",
                  opacity: '0.6'
                }}
              >
                <p className="font-weight-bold">{sp.name}</p>
                <p>{sp.price}$</p>
                <p>{sp.desc}</p>
              </div> */}
            </div>
            
          </div>
        </div>
        <div className="row mx-auto bg-light align-items-center" style={{ width: "500px", height: "150px"}}>
          <div className="col-2  " style={{ width: "55px", height: "32px" ,cursor:'pointer'}} onClick={() => handlCheck(0)}>
            <img className="img-fluid img-thumbnail border border-dark" src="./glassesImage/g1.jpg" alt="" />
          </div>
          <div className="col-2 " style={{ width: "55px", height: "32px", cursor:'pointer' }} onClick={() => handlCheck(1)}>
            <img className=" img-thumbnail border border-dark" src="./glassesImage/g2.jpg" alt="" />
          </div>
          <div className="col-2 " style={{ width: "55px", height: "32px", cursor:'pointer' }} onClick={() => handlCheck(2)}>
            <img className=" img-thumbnail border border-dark" src="./glassesImage/g3.jpg" alt="" />
          </div>
          <div className="col-2 " style={{ width: "55px", height: "32px", cursor:'pointer' }} onClick={() => handlCheck(3)}>
            <img className=" img-thumbnail border border-dark" src="./glassesImage/g4.jpg" alt="" />
          </div>
          <div className="col-2 " style={{ width: "55px", height: "32px", cursor:'pointer' }} onClick={() => handlCheck(4)}>
            <img className=" img-thumbnail border border-dark" src="./glassesImage/g5.jpg" alt="" />
          </div>
          <div className="col-2 " style={{ width: "55px", height: "32px", cursor:'pointer' }} onClick={() => handlCheck(5)}>
            <img className=" img-thumbnail border border-dark" src="./glassesImage/g6.jpg" alt="" />
          </div>
          <div className="col-2 " style={{ width: "55px", height: "32px", cursor:'pointer' }} onClick={() => handlCheck(6)}>
            <img className=" img-thumbnail border border-dark" src="./glassesImage/g7.jpg" alt="" />
          </div>
          <div className="col-2 " style={{ width: "55px", height: "32px", cursor:'pointer' }} onClick={() => handlCheck(7)}>
            <img className=" img-thumbnail border border-dark" src="./glassesImage/g8.jpg" alt="" />
          </div>
          <div className="col-2 " style={{ width: "55px", height: "32px", cursor:'pointer' }} onClick={() => handlCheck(8)}>
            <img className=" img-thumbnail border border-dark" src="./glassesImage/g9.jpg" alt="" />
          </div>
          
        </div>
      </div>
    </div>
  );
};

export default BTState;
